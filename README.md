# e3-ioc-YMIR-ChpSy1:SC-IOC-001

YMIR Minichopper IOC

---

## IOC template

This project was generated with the [E3 cookiecutter IOC template](https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc).

This README.md should be updated as part of creation and should add complementary information about the IOC in question (hardware controlled, addresses, etc.).
