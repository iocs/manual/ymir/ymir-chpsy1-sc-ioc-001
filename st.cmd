#!/usr/bin/env iocsh.bash
require essioc
require(modbus)
require(s7plc)
require(calc)

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet(YMIR-ChpSy1:Chop-CHIC-001_VERSION,"plcfactory")

iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

iocshLoad("./ymir_chpsy1_sc_ioc_001.iocsh","IPADDR=10.102.10.35,RECVTIMEOUT=3000")
